// var createError = require('http-errors');
// var express = require('express');
// var path = require('path');
// var cookieParser = require('cookie-parser');
// var logger = require('morgan');
// const bodyParser = require('body-parser');
// const cors = require('cors');

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');

// var app = express();

// // view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');

// app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));
// app.use(cors());
// app.use('/namedata', indexRouter);
// app.use('/users', usersRouter);

// // catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   // console.log(req, res);
//   next(createError(404));
// });

// // error handler
// app.use(function (err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });


// //  CORS Function
// /* app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "http://localhost:4200");
//   res.header('Access-Control-Allow-Methods', 'GET, POST, UPDATE, DELETE, PUT');
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// }); */
// module.exports = app;


const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const logger = require('morgan');
const app = express();
const jsonParser = bodyParser.json();
const contacts = [
  /* { firstName: 'Divya', lastName: 'Dave', Company: 'knoxpo', jobTitle: 'Web', email: 'divyadave005@gmail.com', phone: 910934567, notes: 'This is my contact form' },
  { firstName: 'Seema', lastName: 'Dave', Company: 'abcd', jobTitle: 'Web', email: 'seema@gmail.com', phone: 9134234568, notes: 'Seema Contact Form' },
  { firstName: 'Akash', lastName: 'Shah', Company: 'enterslice', jobTitle: 'Marketer', email: 'askash005@gmail.com', phone: 9123489765, notes: 'This is askash contact form' } */

]

app.use(cors());
app.use(logger('dev'));

/* app.get('/', function (req, res) {
  res.send('Hello World')
}) */

app.post('/data', jsonParser, function (req, res) {
  console.log(req.body);
  res.json({
    status: 200,
    message: req.body.firstName + req.body.lastName
  });
})
app.post('/addContact', jsonParser, function (req, res) {
  console.log(req.body);
  /* if (!req.body.firstName || !req.body.lastName || !req.body.phone.toString().match(/^[0-9]$/g)) {
    res.status(404);
    res.json({ 'message': 'Bad Request' });
  } */
  /*  else { */
  contacts.push({
    id: contacts.length + 1,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    company: req.body.company,
    jobTitle: req.body.jobTitle,
    email: req.body.email,
    phone: req.body.phone,
    notes: req.body.notes,
  });
  res.json('The Records are added');
});
/* }); */

app.get('/list', function (req, res) {
  if (contacts.length !== 0) {
    res.send(contacts.map(contact => contact));
    /* for (let i = 0; i <= contacts.length; i++) {
      console.log(contacts.length);
      console.log("Array is ", contacts[i]);
      res.send(contacts[i]);
    } */

  }
  else {
    res.status(404);
    res.json({ 'message': 'Bad Request' });
  }

});
app.put('/edit/:id', function (req, res) {
  const updateIndex = contacts.find(contact => contact.id);
  if (updateIndex === -1) {
    contacts.push({
      id: req.params.id,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      company: req.body.company,
      jobTitle: req.body.jobTitle,
      email: req.body.email,
      phone: req.body.phone,
      notes: req.body.notes
    })
  }
  else {
    contacts[updateIndex] = {
      id: req.params.id,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      company: req.body.company,
      jobTitle: req.body.jobTitle,
      email: req.body.email,
      phone: req.body.phone,
      notes: req.body.notes
    }
    res.json({ 'message': 'Record edited Successfully' });
  }
});
app.delete('/delete/:id', (req, res) => {
  const requestId = req.params.id;
  let contact = contacts.filter(contact => {
    return contact.id == requestId
  })[0];
  const newIndex = contacts.indexOf(contact);
  contacts.splice(newIndex, 1);
  res.json({ 'message': `User Deleted at ${requestId}` });
})

app.listen(3000)